const { product } = require("../../models");

async function getProducts(req, res) {
    const productsData = await product.findAll()
    res.render("index", {
        layouts :'layouts/_mainContent',
        productsData,
    });
};
async function addProductPage(req, res) {
    const productsData = await product.findAll()
    res.render("index", {
        layouts :'layouts/create',
        productsData,
    });
};
async function updateProductPage(req, res) {
    const productsData = await product.findAll()
    res.render("index", {
        layouts :'layouts/create',
        productsData,
    });
};

module.exports = {
    getProducts,
    addProductPage,
    updateProductPage,
}