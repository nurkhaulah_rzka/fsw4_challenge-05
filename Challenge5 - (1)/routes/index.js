const router = require("express").Router();

const productController = require('../controllers/productController')
const adminProductController = require('../controllers/admin/productController')

// API
router.get('/api/products', productController.getProducts)
router.post('/api/products', productController.createProduct)
router.get('/api/products/:id', productController.getProductById)
router.put('/api/products/:id', productController.updateProduct)
router.delete('/api/products/:id', productController.deleteProduct)

// dashboard admin
router.get('/', adminProductController.getProducts)
router.get('/addData', adminProductController.addProductPage)
router.get('/update', adminProductController.updateProductPage)



module.exports = router