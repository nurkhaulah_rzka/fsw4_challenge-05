// event will be executed when the toggle-button is clicked
document.getElementById("button-toggle").addEventListener("click", () => {

// when the button-toggle is clicked, it will add/remove the active-sidebar class
document.getElementById("sidenav").classList.toggle("active-sidenav");
document.getElementById("list-menu").classList.toggle("active-list-menu");

// when the button-toggle is clicked, it will add/remove the active-main-content class
document.getElementById("main-content").classList.toggle("active-main-content");
});
