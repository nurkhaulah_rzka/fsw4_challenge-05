# FSW4 Binar: Challenge Chapter 5
`by: @nurkhaulah_rzka`
- Modifikasi nama dan password database pada file `.env`, sesuaikan dengan database di postgres Anda.
- Modifikasi folder `view` apabila ingin memodifikasi tampilan.

# `Noted`

Folder `Challenge5 - (1)` berisi file challenge yang belum final (belum terdapat fungsi search, dan belum bisa add data).
Sedangkan Folder `Challenge5 - (2)` berisi file challenge yang sementara sudah final (berhasil add data, namun belum berhasil search).
Untuk mengetes tampilan hasil, silahkan langsung buka/jalankan folder `Challenge5 - (2)`.

# Tips

Read me first!

