const router = require("express").Router();
const uploader = require('../middleware/uploader')
const productController = require('../controllers/productController')
const adminProductController = require('../controllers/admin/productController')

// API
router.get('/api/products', productController.getProducts)
router.post('/api/products',uploader.single('imageUrl'), productController.createProduct)
router.get('/api/products/:id', productController.getProductById)
router.put('/api/products/:id',uploader.single('imageUrl'), productController.updateProduct)
router.delete('/api/products/:id', productController.deleteProduct)

// dashboard admin
router.get('/', adminProductController.getProducts)
router.get('/addData', adminProductController.addProductPage)
router.post('/addData',uploader.single('image'), adminProductController.addProduct)
router.get('/update/:id', adminProductController.updateProductPage)
router.post('/update/:id', uploader.single('image') , adminProductController.updateProduct)
router.get('/search', adminProductController.searchProduct)

// 404 page
router.use('/',adminProductController.pageNotFound)

module.exports = router