const { product } = require("../models");
const imagekit = require("../lib/imagekit");

// get atau retrieve function controller
async function getProducts(req, res) {
    try {
        const responseData = await product.findAll()
        res.status(200).json({
            'status': 'success',
            'totalData':responseData.length,
            'data': responseData,
        })
    } catch (err) {
        console.log(err.message)
    }
}

// create new data
async function createProduct(req, res) {
    try {
        // process file naming        
        const split = req.file.originalname.split('.')
        const extension = split[split.length - 1]

        const imageName = req.file.originalname

        // upload file 
        const img = await imagekit.upload({
            file: req.file.buffer,
            fileName: imageName,
        })

        console.log(img.url)

        const { name, rent, size } = req.body

        const newProduct = await product.create({ 
            name: name,
            rent: rent,
            imageUrl: img.url,
            size: size,
        })
        
        res.status(200).json({
            'status': 'success',
            'data': newProduct
        })
    } catch (err) {
        res.status(400).json({
            'message': err.message
        })
    }
}

// get atau retrieve function controller
async function getProductById(req, res) {
    try {
        const id = req.params.id
        const item = await product.findByPk(id)
        // console.log(product)
        if (item === null) {
            res.status(404).json({
                'message': `data pada ${id} tersebut tidak ada`
            })
        }

        res.status(200).json({
            'data': item
        })
    } catch (err) {
        console.log(err.message)
    }
}

// update data
async function updateProduct(req, res) {
    try {
        const imageName = req.file.originalname
        // upload file 
        const img = await imagekit.upload({
            file: req.file.buffer,
            fileName: imageName
        })

        const imageUrl = img.url

        const {name, rent, size} = req.body
        const id = req.params.id
        const item = await product.update({
            name,
            rent,
            size,
            imageUrl,
        }, {
            where: {
                id
            }
        })

        res.status(200).json({
            'success': true,
        })
    } catch (err) {
        console.log(err.message)
    }
}

// delete data
async function deleteProduct(req, res) {
    try {
        const id = req.params.id
        await product.destroy({
            where: {
                id
            }
        })

        res.status(200).json({
            'message': 'success delete produk'
        })
    } catch (err) {
        console.log(err.message)
    }
}


module.exports = {
    getProducts,
    createProduct,
    getProductById,
    updateProduct,
    deleteProduct
}