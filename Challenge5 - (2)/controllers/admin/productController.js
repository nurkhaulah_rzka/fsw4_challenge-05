const { product } = require("../../models");
const imagekit = require("../../lib/imagekit");
const { Op, like } = require("sequelize");

async function getProducts(req, res) {
    const size = req.query.filter
    if(size==null) {
        const productsData = await product.findAll()
        res.render("index", {
            layouts :'layouts/_mainContent',
            productsData,
        });
    }else {
        const productsData = await product.findOne({ where: { size } });
        res.render("index", {
            layouts :'layouts/_mainContent',
            productsData,
        });
    }
};
async function searchProduct(req, res) {
    let productsData = await product.findAll({
        where: {
            name: {
              [Op.like]: `${req.query.name}`
            }
          }
    })
    res.render("index", {
        layouts :'layouts/_mainContent',
        productsData,
    });
};
async function addProductPage(req, res) {
    const productsData = await product.findAll()
    res.render("index", {
        layouts :'layouts/create',
        productsData,
    });
};
async function updateProductPage(req, res) {
    const id = req.params.id
    const productsData = await product.findByPk(id)
    res.render("index", {
        layouts :'layouts/update',
        productsData,
    });
};

async function addProduct(req, res) {
    const imageName = req.file.originalname
    // upload file 
    const img = await imagekit.upload({
        file: req.file.buffer,
        fileName: imageName,
    })
    const imageUrl = img.url
    const { name, rent, size } = req.body
    await product.create({ 
        name: name,
        rent: rent,
        imageUrl: imageUrl,
        size: size,
    })
    res.redirect("/")
    // const productsData = await product.findAll()
    // const alert = '<div class="alert alert-success" role="alert">
//     A simple success alert with <a href="#" class="alert-link">an example link</a>. Give it a click if you like.
//   </div>'
//     res.render("index", {
//         layouts :'layouts/create',
//         alert,
//         productsData,
//     });
};

async function updateProduct(req, res) {

    const productsData = await product.findAll()
    res.render("index", {
        layouts :'layouts/update',
        productsData,
    });
};

// 404 Page
async function pageNotFound(req, res) {
    res.render("404");
};

module.exports = {
    getProducts,
    addProductPage,
    addProduct,
    updateProductPage,
    updateProduct,
    searchProduct,
    pageNotFound,
}